// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    ((1.0f64/5.0f64.sqrt())*(((1.0f64+5.0f64.sqrt())/2.0f64).powf(n.into()) - ((1.0f64 - 5.0f64.sqrt())/2.0f64).powf(n.into()))) as u32       
}


fn main() {
    println!("{}", fibonacci_number(10));
}
